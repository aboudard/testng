import { Injectable } from '@angular/core';
import { EntityCollectionServiceBase, EntityCollectionServiceElementsFactory } from '@ngrx/data';
import { Boat } from '../dto/boat';

@Injectable({
  providedIn: 'root'
})
export class BoatsService extends EntityCollectionServiceBase<Boat> {

  constructor(serviceElementsFactory: EntityCollectionServiceElementsFactory) {
    super('Boat', serviceElementsFactory);
  }
}
