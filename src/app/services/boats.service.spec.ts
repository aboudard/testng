import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { EntityDataModule } from '@ngrx/data';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { entityConfig } from '../store/entity-metadata';
import { BoatsService } from './boats.service';


describe('BoatsService', () => {
  let service: BoatsService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, EffectsModule.forRoot([]), StoreModule.forRoot({}), EntityDataModule.forRoot(entityConfig)],
    });
    service = TestBed.inject(BoatsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
