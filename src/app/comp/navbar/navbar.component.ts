import { Component, Input } from '@angular/core';
import { Link } from 'src/app/dto/link';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent {

  @Input()
  items: Array<Link> = [];

  @Input()
  title: string = "";

}
