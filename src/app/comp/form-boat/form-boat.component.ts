import { Component, EventEmitter, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { STEPS } from 'src/app/shared/config-form';
import { Boat } from '../../dto/boat';
import { CATEGORIES } from '../../shared/config-form';

@Component({
  selector: 'app-form-boat',
  templateUrl: './form-boat.component.html',
  styleUrls: ['./form-boat.component.scss']
})
export class FormBoatComponent {

  steps = STEPS;
  categories = CATEGORIES;
  activeStep: number = 0;
  formBoat: FormGroup = new FormGroup({
    nom: new FormControl('', Validators.required),
    propriete: new FormControl('', Validators.required),
    categorie: new FormControl('', Validators.required),
    longueur: new FormControl('', Validators.required),
    annexe: new FormControl(false, Validators.required)
  });
  @Output() readonly formSubmit: EventEmitter<Boat> = new EventEmitter<Boat>();

  get nom() { return this.formBoat.get('nom'); }
  get propriete() { return this.formBoat.get('propriete'); }
  get categorie() { return this.formBoat.get('categorie'); }
  get longueur() { return this.formBoat.get('longueur'); }
  get annexe() { return this.formBoat.get('annexe'); }

  goToStep(step?: string): void {
    const boatCat = this.categorie?.value;
    if (step === "prev") {
      this.activeStep -= 1;
    } else {
      if (this.activeStep === 2 || this.activeStep === 3 ||
        (this.activeStep === 1 && (boatCat !== 'catamaran' && boatCat !== 'voilier'))) {
        this.activeStep = 4;
      } else if (boatCat === 'catamaran' && this.activeStep === 1) {
        this.activeStep = 2;
      } else if (boatCat === 'voilier' && this.activeStep === 1) {
        this.activeStep = 3;
      } else {
        this.activeStep += 1;
      }
    }

  }

  submitBoat() {
    console.log(this.formBoat.value);
    this.formSubmit.emit(this.formBoat.value);
}

}
