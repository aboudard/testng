import { Component, EventEmitter, Output } from '@angular/core';
import { FormControl } from '@angular/forms';
import { debounceTime, startWith } from 'rxjs/operators';

@Component({
  selector: 'app-boat-filter',
  templateUrl: './boat-filter.component.html',
  styleUrls: ['./boat-filter.component.scss']
})
export class BoatFilterComponent {

  filter: FormControl = new FormControl;

  @Output()
  goFilter = new EventEmitter<string>();

  constructor() {
    this.filter.valueChanges.pipe(
      startWith(''),
      debounceTime(500)
      ).subscribe(res => this.goFilter.emit(res));
  }



}
