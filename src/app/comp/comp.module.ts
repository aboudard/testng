import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { BoatFilterComponent } from './boat-filter/boat-filter.component';
import { BoatListComponent } from './boat-list/boat-list.component';
import { FormBoatComponent } from './form-boat/form-boat.component';
import { NavbarComponent } from './navbar/navbar.component';



@NgModule({
  declarations: [
    BoatListComponent,
    BoatFilterComponent,
    FormBoatComponent,
    NavbarComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    RouterModule
  ],
  exports: [
    BoatListComponent,
    BoatFilterComponent,
    FormBoatComponent,
    NavbarComponent
  ]
})
export class CompModule { }
