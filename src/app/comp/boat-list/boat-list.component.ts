import { Component, Input } from '@angular/core';
import { Boat } from '../../dto/boat';

@Component({
  selector: 'app-boat-list',
  templateUrl: './boat-list.component.html',
  styleUrls: ['./boat-list.component.scss']
})
export class BoatListComponent {

  @Input()
  boats: Boat[] = [];

  detailBoat(id: number | undefined): void {
    throw new Error(`not implemented yet with id ${id}`);
  }

}
