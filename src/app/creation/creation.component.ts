import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Boat } from '../dto/boat';
import { BoatsService } from '../services/boats.service';

@Component({
  selector: 'app-creation',
  templateUrl: './creation.component.html',
  styleUrls: ['./creation.component.scss']
})
export class CreationComponent {

  constructor(
    private boatService: BoatsService,
    private router: Router
    ) { }

  addBoat(boat: Boat): void {
    this.boatService.add(boat).subscribe(() => this.router.navigateByUrl('home'));
  }

}
