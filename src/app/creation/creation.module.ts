import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CompModule } from '../comp/comp.module';
import { CreationRoutingModule } from './creation-routing.module';
import { CreationComponent } from './creation.component';



@NgModule({
  declarations: [
    CreationComponent
  ],
  imports: [
    CommonModule,
    CompModule,
    CreationRoutingModule
  ]
})
export class CreationModule { }
