import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';
import { CompModule } from '../comp/comp.module';
import { Boat } from '../dto/boat';
import { HomeComponent } from '../home/home.component';
import { BoatsService } from '../services/boats.service';
import { CreationComponent } from './creation.component';


describe('CreationComponent', () => {

  let boatsServiceSpy: jasmine.SpyObj<BoatsService>;
  let routerSpy: jasmine.SpyObj<Router>;
  let component: CreationComponent;
  let fixture: ComponentFixture<CreationComponent>;
  const mockBoat: Boat = {
    nom: 'gwen bleiz',
    categorie: 'catamaran',
    propriete: 'locataire',
    longueur: 28
  };

  beforeEach(async () => {

    const spy = jasmine.createSpyObj('BoatsService', ['add']);
    const spyRouter = jasmine.createSpyObj('Router', ['navigateByUrl']);

    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule.withRoutes([{path: 'home', component: HomeComponent}]),
        CompModule
      ],
      declarations: [ CreationComponent ],
      providers: [
        { provide: BoatsService, useValue: spy },
        { provide: Router, useValue: spyRouter }
      ]
    })
    .compileComponents();
    boatsServiceSpy = TestBed.inject(BoatsService) as jasmine.SpyObj<BoatsService>;
    boatsServiceSpy.add.and.returnValue(of({...mockBoat, id: 0}));
    routerSpy = TestBed.inject(Router) as jasmine.SpyObj<Router>;
    routerSpy.navigateByUrl.and.callThrough();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call addBoat', () => {
    component.addBoat(mockBoat);
    expect(boatsServiceSpy.add).toHaveBeenCalledWith(mockBoat);
    expect(routerSpy.navigateByUrl).toHaveBeenCalledWith('home');
  });

});
