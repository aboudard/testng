import { EntityMetadataMap, EntityDataModuleConfig } from '@ngrx/data';

const entityMetadata: EntityMetadataMap = {
  Boat: {}
};

const pluralNames = {
  Boat: 'Boat'
};

export const entityConfig: EntityDataModuleConfig = {
  entityMetadata,
  pluralNames
};
