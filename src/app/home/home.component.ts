import { Component } from '@angular/core';
import { BehaviorSubject, combineLatest, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Boat } from '../dto/boat';
import { BoatsService } from '../services/boats.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent {

  filterSubject = new BehaviorSubject('');
  counter$: Observable<number>;
  boats$: Observable<Boat[]>;
  filteredBoats$: Observable<Boat[]>;

  constructor(private boatService: BoatsService) {
    this.counter$ = boatService.count$;
    this.boats$ = boatService.entities$;
    this.filteredBoats$ = combineLatest([this.boats$, this.filterSubject.asObservable()]).pipe(
      map(([boats, filter]) => boats.filter(boat => {
        return Object.values(boat).some((el) => el.toString().toLowerCase().indexOf(filter.toLowerCase()) !== -1);
      }))
    );
  }

  updateFilter(f: string): void {
    this.filterSubject.next(f);
  }

}
