import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CompModule } from '../comp/comp.module';
import { BoatsService } from '../services/boats.service';
import { HomeComponent } from './home.component';


describe('HomeComponent', () => {

  let boatsServiceSpy: jasmine.SpyObj<BoatsService>;
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;

  beforeEach(async () => {

    const spy = jasmine.createSpyObj('BoatsService', ['getAll']);

    await TestBed.configureTestingModule({
      declarations: [ HomeComponent ],
      imports: [
        CompModule
      ],
      providers: [
        { provide: BoatsService, useValue: spy }
      ]
    })
    .compileComponents();
    boatsServiceSpy = TestBed.inject(BoatsService) as jasmine.SpyObj<BoatsService>;

  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

});
