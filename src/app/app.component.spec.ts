import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';
import { AppComponent } from './app.component';
import { CompModule } from './comp/comp.module';
import { BoatsService } from './services/boats.service';

describe('AppComponent', () => {

  let boatsServiceSpy: jasmine.SpyObj<BoatsService>;
  let fixture: ComponentFixture<AppComponent>;
  let app: AppComponent;

  beforeEach(async () => {

    const spy = jasmine.createSpyObj('BoatsService', ['getAll']);

    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        CompModule
      ],
      declarations: [
        AppComponent
      ],
      providers: [
        { provide: BoatsService, useValue: spy }
      ]
    }).compileComponents();
    boatsServiceSpy = TestBed.inject(BoatsService) as jasmine.SpyObj<BoatsService>;
    boatsServiceSpy.getAll.and.returnValue(of([{
      nom: 'gwen bleiz',
      categorie: 'catamaran',
      propriete: 'locataire',
      longueur: 28
    }]));
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AppComponent);
    app = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create the app', () => {
    expect(app).toBeTruthy();
  });

  it(`should have a title`, () => {
    expect(app.title).toEqual('Click&Boat - Location de bateaux');
  });

  it('should call getAll', () => {
    expect(boatsServiceSpy.getAll).toHaveBeenCalled();
  });

});
