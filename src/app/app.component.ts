import { Component, OnInit } from '@angular/core';
import { Link } from './dto/link';
import { BoatsService } from './services/boats.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  title = 'Click&Boat - Location de bateaux';
  links: Array<Link> = [
    {url: 'home', label: 'Accueil'},
    {url: 'creation', label: 'Création'}
  ];

  constructor(private boatService: BoatsService) {}

  ngOnInit(): void {
    this.boatService.getAll();
  }

}
