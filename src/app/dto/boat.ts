export interface Boat {
  id?: number;
  nom: string;
  propriete: string;
  categorie: string;
  longueur?: number;
  annexe?: boolean;
}
