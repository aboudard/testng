export const CATEGORIES = [
  "voilier",
  "catamaran",
  "semin rigide",
  "bateau moteur",
  "peniche",
  "jet-ski",
  "yacht"
]
export const STEPS = [
  {label: "Je suis"},
  {label: "Votre bateau"},
  {label: "Détails Catamaran"},
  {label: "Détails Voilier"},
  {label: "Validation"}
]
