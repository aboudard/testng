describe('My First Test', () => {

  beforeEach(() => {
    cy.visit('/');
  });

  it('Visits the initial project page', () => {
    cy.get('.navbar__title').contains('Click&Boat - Location de bateaux');
    cy.url().should('include', '/home');
  });

  it('should navigate to creation', () => {
    cy.get('#navbar_link__creation').click();
    cy.url().should('include', '/creation');
    cy.get('#btn_step__0').should('be.disabled');
  });

  it('should activate button', () => {
    cy.get('#navbar_link__creation').click();
    cy.get('[for="particulier"]').click();
    cy.get('[name="nom"]').type('Gwen Ha Du');
    cy.get('#btn_step__0').should('be.enabled');
  });

})
